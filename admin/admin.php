<?php
/**
 * Admin functions Gasoline Framework
 **/
add_action('admin_menu', 'gasoline_framework_admin_menu_setup'); //menu setup
add_action('admin_enqueue_scripts', 'gasoline_framework_admin_enqueue');

  
function gasoline_framework_admin_menu_setup(){
	add_submenu_page(
	 'options-general.php',
	 'Gasoline Framework',
	 'Gasoline Framework',
	 'manage_options',
	 'gasoline_framework',
	 'gasoline_framework_menu_render'
	 );	
}


/* display page content */
function gasoline_framework_menu_render() {
	global $submenu;
	?>
		<div class="wrap">		
		<h2><?php echo GASOLINE_FRAMEWORK_NAME?></h2>
		<p>Versão: <?php echo GASOLINE_FRAMEWORK_VERSION?></p>
		<p>Data: <?php echo GASOLINE_FRAMEWORK_DATE?></p>
		<p>Suporte: contato@gasoline-digital.com</p>
		<p>Desenvolvido por: Gasoline Digital - @gasolinedigital - www.gasoline-digital.com</p>
		<p>Atualizações: <a href="https://bitbucket.org/gasolinedeveloper/gasoline.digital.framework" target="_blank">https://bitbucket.org/gasolinedeveloper/gasoline.digital.framework</a></p>
		</div>
	<?php
}

function gasoline_framework_admin_enqueue($hook_suffix) {
	global $typenow; 
	$plugindir = GASOLINE_FRAMEWORK_URL;
	wp_register_style( 'gd_framework_css', $plugindir.'/assets/css/gasoline.digital.framework.css', false, '1.0.0' );
	wp_enqueue_style( 'gd_framework_css' );
}

