=== Gasoline.Digital.Framework ===
Contributors: Gasoline Digital Team
Tags: framework
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 4.0
License: GPLv2 or later

Pack functions and libraries for developing themes and plugins

== Description ==

Pack functions and libraries for developing themes and plugins

== Changelog ==

= 3.0 =
*Release Date - 15 June 2017*

* Release first public version
 