<?php
/**
 * Main functionality
 **/

/** Hook plugin's action and filters **/
function gasoline_framework_core_init(){

	// Helpers
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/date_helper.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/harddata_helper.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/string_helper.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/timezone_helper.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/validation_helper.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/helpers/wordpress_helper.php');
	
	// Libraries
	require_once(GASOLINE_FRAMEWORK_DIR.'core/libraries/output_library.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/libraries/mail_library.php');
	require_once(GASOLINE_FRAMEWORK_DIR.'core/libraries/mailchimp_library.php');	
	
	// Shortcodes
	// Next Updates
}

add_action('plugins_loaded', 'gasoline_framework_core_init',1);
