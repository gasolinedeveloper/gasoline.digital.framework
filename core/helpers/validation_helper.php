<?php
	/*
		Extend Wordpress Functions
		Author: @GasolineDigital
		Helper: Validation
	*/
	
	/*
		Function Name: gd_vd_check_email
		Description: verify email format
	*/
	function gd_vd_check_email($mail){
		if(preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(\.[[:lower:]]{2,3})(\.[[:lower:]]{2})?$/", $mail)) {
			return true;
		}else{
			return false;
		}
	}		

	/*
		Function Name: gd_vd_in_object
		Description:  to check if a value in an object exists.
	*/
	function gd_vd_in_object($value,$object_array) {
		if(is_array($object_array)){
			foreach($object_array as $object){
				if (is_object($object)) {
					foreach($object as $key => $item) {
						if ($value==$item) return $key;
					}
				}
			}
		}
		return false;
	}

