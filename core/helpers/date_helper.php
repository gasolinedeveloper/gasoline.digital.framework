<?php
	/*
		Extend Wordpress Functions
		Author: @GasolineDigital
		Helper: Date
	*/
	
	/**
	* Convert MySQL's DATE (YYYY-MM-DD) or DATETIME (YYYY-MM-DD hh:mm:ss) to timestamp
	* Returns the timestamp equivalent of a given DATE/DATETIME
	*/
	function gd_mysqldatetime_to_timestamp($datetime = "")
	{
	  // function is only applicable for valid MySQL DATETIME (19 characters) and DATE (10 characters)
	  $l = strlen($datetime);
	    if(!($l == 10 || $l == 19))
	      return 0;
	
	    //
	    $date = $datetime;
	    $hours = 0;
	    $minutes = 0;
	    $seconds = 0;
	
	    // DATETIME only
	    if($l == 19)
	    {
	      list($date, $time) = explode(" ", $datetime);
	      list($hours, $minutes, $seconds) = explode(":", $time);
	    }
		
		
	    list($year, $month, $day) = explode("-", $date);
		
	    return mktime($hours, $minutes, $seconds, $month, $day, $year);
		
	}
	
	/**
	* Convert MySQL's DATE (YYYY-MM-DD) or DATETIME (YYYY-MM-DD hh:mm:ss) to date using given format string
	*
	* Returns the date (format according to given string) of a given DATE/DATETIME
	*/
	function gd_mysqldatetime_to_date($datetime = "", $format = "d/m/Y H:i:s")
	{
	    if(!empty($datetime)){
			return date($format, gd_mysqldatetime_to_timestamp($datetime));
		}else{
			return '';	
		}
	}
	
	/**
	* Convert timestamp to MySQL's DATE or DATETIME (YYYY-MM-DD hh:mm:ss)
	*
	* Returns the DATE or DATETIME equivalent of a given timestamp
	*/
	function gd_timestamp_to_mysqldatetime($timestamp = "", $datetime = true)
	{
	  if(empty($timestamp) || !is_numeric($timestamp)) $timestamp = time();
	
	    return ($datetime) ? date("Y-m-d H:i:s", $timestamp) : date("Y-m-d", $timestamp);
	}
	
	/**
	* Convert timestamp to Human Date
	*
	* Returns the date (format according to given string) of a given timestamp
	*/
	function gd_timestamp_to_date($timestamp = "", $format = "d/m/Y H:i:s")
	{
	  if(empty($timestamp) || !is_numeric($timestamp)) $timestamp = time();
	  return date($format, $timestamp);
	}
	
	/**
	* Convert Human Date to Timestamp
	*
	* Returns the timestamp equivalent of a given HUMAN DATE/DATETIME
	*/
	function gd_date_to_timestamp($datetime = "")
	{
	  if (!preg_match("/^(\d{1,2})[.\- \/](\d{1,2})[.\- \/](\d{2}(\d{2})?)( (\d{1,2}):(\d{1,2})(:(\d{1,2}))?)?$/", $datetime, $date))
	    return FALSE;
	
	  $day = $date[1];
	  $month = $date[2];
	  $year = $date[3];
	  $hour = (empty($date[6])) ? 0 : $date[6];
	  $min = (empty($date[7])) ? 0 : $date[7];
	  $sec = (empty($date[9])) ? 0 : $date[9];
	
	  return mktime($hour, $min, $sec, $month, $day, $year);
	}
	
	/**
	* Convert HUMAN DATE to MySQL's DATE or DATETIME (YYYY-MM-DD hh:mm:ss)
	*
	* Returns the DATE or DATETIME equivalent of a given HUMAN DATE/DATETIME
	*/
	function gd_date_to_mysqldatetime($date = "", $datetime = TRUE)
	{
	  return gd_timestamp_to_mysqldatetime(gd_date_to_timestamp($date), $datetime);
	}	
	
	
	/*
		Function Name: gasoline_get_day
		Description: separate day within the full date
	*/
	function gd_get_day($date){
		$date_hour = explode(' ',$date);
		$date_comps = explode('-',$date_hour[0]);
		return $date_comps[2];
	}




	/*
		Function Name: gasoline_get_month
		Description: separate month within the full date
	*/
	function gd_get_month($date){
		$date_hour = explode(' ',$date);
		$date_comps = explode('-',$date_hour[0]);
		return $date_comps[1];
	}


	/*
		Function Name: gasoline_get_month_name
		Description: get month name by number
	*/
	function gd_get_month_name($month_num = 0){
		$month_num = (int)$month_num;
		$month = array('','Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez');
		return $month[$month_num];
	}



	/*
		Function Name: gasoline_get_year
		Description: separate year within the full date
	*/
	function gd_get_year($date){
		$date_hour = explode(' ',$date);
		$date_comps = explode('-',$date_hour[0]);
		return $date_comps[0];
	}

	/*
		Function Name: gasoline_get_hour
		Description: separate hour within the full date
	*/
	function gd_get_hour($date){
		$date_hour = explode(' ',$date);
		$date_comps = explode(':',$date_hour[1]);
		return $date_comps[0];
	}


	/*
		Function Name: gasoline_get_minutes
		Description: separate minutes  within the full date
	*/
	function gd_get_minutes($date){
		$date_hour = explode(' ',$date);
		$date_comps = explode(':',$date_hour[1]);
		return $date_comps[1];
	}	
