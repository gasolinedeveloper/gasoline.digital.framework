<?php
	/*
		Extend Wordpress Functions
		Author: @GasolineDigital
		Helper: Timezone
	*/
	
	/*
		Function Name: gd_tz_switch_timezone
		Description: Change Timezone with return actual timezone
	*/
	function gd_tz_switch_timezone($timezone = 'America/Sao_Paulo'){
		$default_timezone = date_default_timezone_get();
		date_default_timezone_set($timezone);
		return $default_timezone; 
	}	