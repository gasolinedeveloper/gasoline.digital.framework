<?php
	/*
		Extend Wordpress Functions
		Author: @GasolineDigital
		Helper: Wordpress
	*/
	
	/*
		Function Name: gd_wp_get_image
		Description: get image attached 
	*/	
	function gd_wp_get_image($id){
		return wp_get_attachment_url( get_post_thumbnail_id($id) );
	}
	
	/*
		Function Name: gd_wp_make_title
		Description: make title with filter
	*/		
	function gd_wp_make_title($string){
		return apply_filters('the_title', $string);
	}

	/*
		Function Name: gd_wp_make_content
		Description: make content with filter
	*/	
	function gd_wp_make_content($string){
		return apply_filters('the_content', $string);
	}
	
	/*
		Function Name: gd_wp_make_excerpt
		Description: make excerpt with filter
	*/		
	function gd_wp_make_excerpt($limit, $excerpt = "",$final='...'){
	
	    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	    $excerpt = strip_shortcodes($excerpt);
	    $excerpt = strip_tags($excerpt);
	    if(strlen($excerpt) > $limit){
		    $excerpt = substr($excerpt, 0, $limit);
		    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
		    $excerpt = $excerpt.$final;
	    }
	    return $excerpt;
	}	
	
	/*
		Function Name: gd_wp_the_excerpt
		Description:  get excerpt with filter
	*/		
	function gd_wp_the_excerpt($post,$limit = 120){
		if(empty($post->post_excerpt)){
			return gd_wp_make_excerpt($limit,$post->post_content);
		}else{
			if(strlen($post->post_excerpt) > $limit){
				return gd_wp_make_excerpt($limit,$post->post_excerpt);
			}else{
				return $post->post_excerpt;	
			}			
		}
	}

	/*
		Function Name: gd_wp_title_simple
		Description:  get page title simple
	*/		
	function gd_wp_title_simple(){
		global $page, $paged;
	
		wp_title( '|', true, 'right' );
	
		// Add the blog name.
		bloginfo( 'name' );
	
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
	
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __('Page %s'), max( $paged, $page ) );		
	} 

	/*
		Function Name: gd_wp_title
		Description:  get page title advanced
	*/		
	function gd_wp_title( $postid = '', $sep = '&raquo;', $seplocation = '' ) {
		if ( ! $postid ) return '';
		$post = get_post($postid);
		if ( ! is_object($post) || ! isset($post->post_title) ) return '';
		$t_sep = '%WP_TITILE_SEP%';
		$title = apply_filters('single_post_title', $post->post_title, $post);
		$prefix = '';
		if ( ! empty($title) ) $prefix = " $sep ";
		if ( 'right' == $seplocation ) { // sep on right, so reverse the order
			$title_array = explode( $t_sep, $title );
			$title_array = array_reverse( $title_array );
			$title = implode( " $sep ", $title_array ) . $prefix;
		} else {
			$title_array = explode( $t_sep, $title );
			$title = $prefix . implode( " $sep ", $title_array );
		}
		return apply_filters('wp_title', $title, $sep, $seplocation);
	}	
	
	/*
		Function Name: gd_wp_description
		Description:  get page description
	*/			
	function gd_wp_description(){
	    if ( is_single() ) {
	        echo get_the_excerpt();
	    } else { 
	    	bloginfo('name'); 
	    	echo " - "; 
	    	bloginfo('description');
		}
	}

	/*
		Function Name: gd_wp_image_head
		Description:  get image for head if single
	*/			
	function gd_wp_image($default){
	     global $post;
		if ( is_single() ) {
	        echo gd_wp_get_image($post->ID);
	    } else { 
	    	echo $default;
		}
	}	
	
	
	/*
		Function Name: gd_wp_is_home
		Description:  Test Is Page Home
	*/			
	function gd_wp_is_home(){
		if ( is_front_page() && is_home() ) {
			return true;	 
		} elseif ( is_front_page() ) {
			return true;
		} elseif ( is_home() ) {
			return true;
		} else {
			return false;
		}
	}

	/*
		Function Name: gd_wp_type_page
		Description:  return page type
	*/	
	function gd_wp_type_page(){
		if(gd_wp_is_home()){
			return 'home';
		}

		if(is_single()){
			return 'single';
		}

		if(is_page()){
			return 'page';
		}

		if(is_category()){
			return 'category';
		}

		if(is_tag()){
			return 'tag';
		}

		if(is_tax()){
			return 'tax';
		}

		if(is_author()){
			return 'author';
		}

		if(is_date()){
			return 'date';
		}

		if(is_archive()){
			return 'archive';
		}

		if(is_search()){
			return 'search';
		}

		if(is_404()){
			return '404';
		}
	}
	

	/*
		Function Name: gd_wp_post_slug
		Description: transform string slug
	*/	
	function gd_wp_post_slug($str){
		return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), gasoline_remove_accent($str)));
	}	
        
	/*
		Function Name: gd_wp_the_breadcrumb
		Description: Make BreadCrumb
	*/        
    function gd_wp_the_breadcrumb() {
        global $post;
        echo '<ul id="breadcrumbs">';
        if (!is_home()) {
            echo '<li><a href="';
            echo get_option('home');
            echo '">';
            echo 'Home';
            echo '</a></li><li class="separator">  »  </li>';
            if (is_category() || is_single()) {
                echo '<li>';
                the_category(' </li><li class="separator">  »  </li><li> ');
                if (is_single()) {
                    echo '</li><li class="separator">  »  </li><li>';
                    the_title();
                    echo '</li>';
                }
            } elseif (is_page()) {
                if($post->post_parent){
                                    $anc = get_post_ancestors( $post->ID );
                                    $title = get_the_title();
                                    foreach ( $anc as $ancestor ) {
                                            $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">/</li>';
                                    }
                                    echo $output;
                                    echo '<strong title="'.$title.'"> '.$title.'</strong>';
                            } else {
                                    echo '<li><strong> '.get_the_title().'</strong></li>';
                            }
            }
        }
        if (is_tag()) {single_tag_title();}
        elseif (is_day()) {echo"<li>Arquivo for "; the_time('F jS, Y'); echo'</li>';}
        elseif (is_month()) {echo"<li>Arquivo for "; the_time('F, Y'); echo'</li>';}
        elseif (is_year()) {echo"<li>Arquivo for "; the_time('Y'); echo'</li>';}
        elseif (is_author()) {echo"<li>Autor Archive"; echo'</li>';}
        elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Arquivos"; echo'</li>';}
        elseif (is_search()) {echo"<li>Resultado da Pesquisa"; echo'</li>';}
        echo '</ul>';
    }        

	/*
		Function Name: get_wp_top_category
		Description: function to get the top level category object
					 Usage - $top_cat = get_top_category();
					 echo $top_cat->slug;
	*/  
	function get_wp_top_category() {
	    $cats = get_the_category(); // category object
	    $top_cat_obj = array();

	    foreach($cats as $cat) {
	        if ($cat->parent == 0) {
	            $top_cat_obj[] = $cat;  
	        }
	    }
	    $top_cat_obj = $top_cat_obj[0];
	    return $top_cat_obj;
	}

	/*
		Function Name: gd_wp_clean_input
		Description: clean string 
	*/  
	function gd_wp_clean_string($input) { 
		$search = array(
			'@<script[^>]*?>.*?</script>@si',   // Strip out javascript
			'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
			'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
			'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		);
	    $output = preg_replace($search, '', $input);
	    return $output;
	}

	/*
		Function Name: gd_wp_sanitize
		Description: sanitize string or array
	*/  		
	function gd_wp_sanitize($input) {
	    if (is_array($input)) {
	        foreach($input as $var=>$val) {
	            $output[$var] = gd_wp_sanitize($val);
	        }
	    }
	    else {
	        if (get_magic_quotes_gpc()) {
	            $input = stripslashes($input);
	        }
	        $input  = gd_wp_clean_string($input);
	        $output = mysql_real_escape_string($input);
	    }
	    return $output;
	}	

	/*
		Function Name: gd_wp_getip
		Description: get user ip
	*/  	
	function gd_wp_getip(){
	    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	    {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	    {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else
	    {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}	