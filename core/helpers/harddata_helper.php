<?php
	/*
		Extend Wordpress Functions
		Author: @GasolineDigital
		Helper: Hard Data
	*/
	

	/*
		Function Name: gd_hd_uf
		Description: get uf
	*/
	function gd_hd_uf($uf_set = ''){
		$state = array(
						''=>' -- ',
						'AC'=>'Acre',
						'AL'=>'Alagoas',
						'AP'=>'Amapá',
						'AM'=>'Amazonas',
						'BA'=>'Bahia',
						'CE'=>'Ceará',
						'DF'=>'Distrito Federal',
						'ES'=>'Espirito Santo',
						'GO'=>'Goiás',
						'MA'=>'Maranhão',
						'MS'=>'Mato Grosso do Sul',
						'MT'=>'Mato Grosso',
						'MG'=>'Minas Gerais',
						'PA'=>'Pará',
						'PB'=>'Paraíba',
						'PR'=>'Paraná',
						'PE'=>'Pernambuco',
						'PI'=>'Piauí',
						'RJ'=>'Rio de Janeiro',
						'RN'=>'Rio Grande do Norte',
						'RS'=>'Rio Grande do Sul',
						'RO'=>'Rondônia',
						'RR'=>'Roraima',
						'SC'=>'Santa Catarina',
						'SP'=>'São Paulo',
						'SE'=>'Sergipe',
						'TO'=>'Tocantins'
		);
		$options = '';
		foreach($state as $key=>$uf){
			if($uf_set == $key){
				$options .= '<option value="'.$key.'" selected="selected">'.$uf.'</option>';		
			}else{
				$options .= '<option value="'.$key.'">'.$uf.'</option>';		
			}
		}
		return $options;
	}			

	/*
		Function Name: gd_hd_month
		Description: get month
	*/
	function gd_hd_month($month_set = ''){
		$months = array(
						''=>' -- ',
						'1'=>'Janeiro',
						'2'=>'Fevereiro',
						'3'=>'Março',
						'4'=>'Abril',
						'5'=>'Maio',
						'6'=>'Junho',
						'7'=>'Julho',
						'8'=>'Agosto',
						'9'=>'Setembro',
						'10'=>'Outubro',
						'11'=>'Novembro',
						'12'=>'Dezembro'
		);
		$options = '';
		foreach($months as $key=>$value){
			if($month_set == $key){
				$options .= '<option value="'.$key.'" selected="selected">'.$value.'</option>';		
			}else{
				$options .= '<option value="'.$key.'">'.$value.'</option>';		
			}
		}
		return $options;
	}		
	
	/*
		Function Name: gd_hd_gender
		Description: get gender
	*/
	function gd_hd_gender($gender_set = ''){
		$gender = array(
						''=>' -- ',
						'M'=>'Masculino',
						'F'=>'Feminino'
		);
		$options = '';
		foreach($gender as $key=>$value){
			if($gender_set == $key){
				$options .= '<option value="'.$key.'" selected="selected">'.$value.'</option>';		
			}else{
				$options .= '<option value="'.$key.'">'.$value.'</option>';		
			}
		}
		return $options;
	}		