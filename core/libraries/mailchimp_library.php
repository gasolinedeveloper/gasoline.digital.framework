<?php
	/*
		Gasoline Digital Libraries Functions
		Author: @GasolineDigital
		Library: GD_MailChimp
	*/
	
	class GD_MailChimp {				
		var $apikey = '';
		var $user = '';
		var $server = 'us15';
		var $version = '3.0';
		var $url = 'https://{server}.api.mailchimp.com/{version}/';
		var $list  = '';
		var $breakline = '\r\n';
		var $log  = false;
		var $data = '';

		function __construct($server = '',$version = ''){	
			if(!empty($server)){
				$this->server = $server;
			}

			if(!empty($version)){
				$this->version = $version;
			}

			$this->url = str_replace('{server}',$this->server,$this->url);
			$this->url = str_replace('{version}',$this->version,$this->url);
		}

		function add_list($email,$values = array('FNAME'=>''), $list = ''){
			try {
				if(empty($list) && empty($this->list)){
					return false;
				}

				if(empty($list)){
					$list = $this->list;
				}

				$auth = base64_encode( 'user:'.$this->apikey);
				
				$data = array(
				    'apikey'        => $this->apikey,
				    'email_address' => $email,
				    'status'        => 'subscribed',
				    'merge_fields'  => $values,
				);

				$this->data = $json_data = json_encode($data);
				
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, $this->url.'lists/'.$list.'/members/');
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json/r/n
				                                            Authorization: Basic '.$auth));
				curl_setopt($ch, CURLOPT_USERPWD, "$this->user:$this->apikey");  
				curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                                                  
				
				$result = curl_exec($ch);
				

				if (FALSE === $result)
        			throw new Exception(curl_error($ch), curl_errno($ch));				

        		curl_close($ch);


				if($this->log){
					// Log path
					$upload_dir = wp_upload_dir();					
					$log_dir = $upload_dir['basedir'].'/gd.logs/';					
					if(!is_dir($log_dir)){
						mkdir($log_dir);
					}

					$log_dir .='/mailchimp/';					
					if(!is_dir($log_dir)){
						mkdir($log_dir);
					}

					$logmsg = "### --- Mailchimp API Log: ".date('d/m/Y h:i:s').$this->breakline;
					$logmsg .= 'Data:'.$this->data.$this->breakline;
					$logmsg .= 'Return:'.$result.$this->breakline;

					$log_file = $log_dir . 'MAILCHIMP_SUCCESS_'.date('Ymdhis').'-'.rand().'.txt';
					$file = fopen($log_file,"w");
					fwrite($file,$logmsg);
					fclose($file);				
				}

				return true;

			} catch (Exception $e) {

				if($this->log){
					// Log path
					$upload_dir = wp_upload_dir();
					$log_dir = $upload_dir['basedir'].'gd.logs/mailchimp/';
					if(!is_dir($log_dir)){
						mkdir($log_dir);
					}

					$logmsg = "### --- Mailchimp API Log: ".date('d/m/Y h:i:s').$this->breakline;
					$logmsg .= 'Data:'.$this->data.$this->breakline;
					$logmsg .= 'Code:'.$e->getCode().$this->breakline;
					$logmsg .= 'Message:'.$e->getMessage().$this->breakline;
					

					$log_file = $log_dir . 'MAILCHIMP_ERROR_'.date('Ymdhis').'-'.rand().'.txt';
					$file = fopen($log_file,"w");
					fwrite($file,$logmsg);
					fclose($file);				
				}


				return false;
			}					
		} 		
	}		