<?php
	/*
		Gasoline Digital Libraries Functions
		Author: @GasolineDigital
		Library: GD_Mail
	*/	
	
	class GD_Mail {
				
		var $mail_sender = '';
		var $mail_to = '';
		var $mail_from  = '';
		var $mail_copy  = '';
		var $mail_co  = '';
		var $subject  = '';
		var $message  = '';
		var $headers = '';
		var $os = 'Linux';
		var $breakline = '\n';
		var $host = 'locaweb';
		var $safe_mode = true;
		var $encoding = false;
		var $charset = 'utf-8';
		var $return_path = false;
		var $log = false;
		
		function __construct(){	
			$this->isOS();
		}				
		
		public function setSender($mail_sender){
			$this->mail_sender = $mail_sender;
		}	

		public function setTo($mail_to){
			$this->mail_to = $mail_to;
		}			

		public function setFrom($mail_from){
			$this->mail_from = $mail_from;
		}			

		public function setCopy($mail_copy){
			$this->mail_copy = $mail_copy;
		}			

		public function setBCC($mail_co){
			$this->mail_co = $mail_co;
		}					
		
		public function setMessage($message){
			if($this->encoding){
				$this->message = htmlspecialchars_decode(htmlentities($message, ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES);
			}else{
				$this->message = $message;
			}
		}							
		
		public function setSubject($subject){
			if($this->encoding){
				$this->subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
			}else{
				$this->subject = $subject;
			}
		}									
		
		public function setOS($os){
			$this->isOS($os);
		}									
		
		public function setSafeMode($safe_mode){
			$this->safe_mode = $safe_mode;
		}											
		
		public function setEncoding($encoding){
			$this->encoding = $encoding;
		}													

		public function setCharset($charset){
			$this->charset = $charset;
		}																	

		public function setReturnPath($return_path){
			$this->host = $return_path;
		}															
		
		public function setHost($host){
			$this->host = $host;
			$this->isHost();
		}															
		
		private function isHost(){
			$host = strtolower($this->host);
			switch ($host) {
				case "locaweb":
					$this->return_path = false;
					$this->safe_mode = true;
					break;
				case "servage":
					$this->return_path = true;
					$this->safe_mode = false;
					$this->encoding = true;
					break;
				case "godaddy":
					$this->return_path = false;
					$this->safe_mode = true;
					break;
			}			
		}
		
		private function isOS($os = PHP_OS){
			if($os == "Linux"){ $this->breakline = "\n"; $this->os = "Linux"; 
			}elseif($os == "WINNT"){ $this->breakline = "\r\n";$this->os = "Windows";
			}else {die("This script is not prepared for this work with the operating system of your server");}					
		}
		
		private function createHeader(){
			$breakline = $this->breakline;
			$charset = $this->charset;
			$headers = "MIME-Version: 1.1".$breakline;
			$headers .= "Content-type: text/html; charset=".$charset . $breakline;
			
			if(!empty($this->mail_sender)){
				$headers .= "From: " . $this->mail_sender . $breakline;
				$headers .= "Return-Path: " . $this->mail_sender . $breakline;
			}
			
			if(!empty($this->mail_copy)){
				$headers .= "Cc: ".$this->mail_copy . $breakline;
			}						
			
			if(!empty($this->mail_co)){
				$headers .= "Bcc: ".$this->mail_co . $breakline;
			}			

			$headers .= "Reply-To: ".$this->mail_from . $breakline;
			$headers .= "X-Priority: 3";
			
			if($this->return_path){
				$headers .= "Return-Path: " . $this->mail_sender . $breakline;
			}
			$this->headers = $headers;
			return $headers;
		}	
		
		public function Send(){
			$breakline = $this->breakline;
			$headers = $this->createHeader();
			$message = $this->message;
			$subject = $this->subject;
			$mail_to = $this->mail_to;
			$mail_from = $this->mail_from;
			$mail_sender = $this->mail_sender;
			

			mail($mail_to, $subject, $message, $headers );

			if($this->log){
				// Log path
				$upload_dir = wp_upload_dir();
				$log_dir = $upload_dir['basedir'].'gd.logs/mail/';
				if(!is_dir($log_dir)){
					mkdir($log_dir);
				}

				$logmsg = "### --- E-mail Log: ".date('d/m/Y h:i:s').$this->breakline;
				$logmsg .= 'Header:'.$headers.$this->breakline;
				$logmsg .= 'Subject:'.$subject.$this->breakline;
				$logmsg .= 'Message:'.$message.$this->breakline;
				

				$log_file = $log_dir . 'MAIL_' .date('Ymdhis').'-'.rand().'txt';
				$file = fopen($log_file,"w");
				fwrite($file,$logmsg);
				fclose($file);				
			}
		}
	}