<?php
/**
 * @package Gasoline.Digital
 */
/*
Plugin Name: Gasoline.Digital Framework
Description: Gasoline.Digital Framework Developer 
Version: 3.0.0
Author: @GasolineDigital
Author URI: http://www.gasoline.digital
Plugin URI: http://www.gasoline.digital
License: GPLv2 or later
Text Domain: Gasoline.Digital.Framework
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2008-2018 Gasoline.Digital, Inc.
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

/**
 * Define some useful constants
 **/
define('GASOLINE_FRAMEWORK_ID','gasoline.digital.framework');
define('GASOLINE_FRAMEWORK_NAME','Gasoline Digital Framework');
define('GASOLINE_FRAMEWORK_DATE', '15/06/2017');
define('GASOLINE_FRAMEWORK_VERSION', '3.0.0');
define('GASOLINE_FRAMEWORK_DIR', plugin_dir_path(__FILE__));
define('GASOLINE_FRAMEWORK_URL', plugin_dir_url(__FILE__));

/**
 * Activation, Deactivation and Uninstall Functions
 * 
 **/
register_activation_hook(__FILE__, 'gasoline_framework_activation');
register_deactivation_hook(__FILE__, 'gasoline_framework_deactivation');

function gasoline_framework_activation() {
	//actions to perform once on plugin activation go here    
    //register uninstaller
    register_uninstall_hook(__FILE__, 'gasoline_framework_uninstall');
}

function gasoline_framework_deactivation() {    
	// actions to perform once on plugin deactivation go here	    
}

function gasoline_framework_uninstall(){    
    //actions to perform once on plugin uninstall go here	    
}

/**
 * Load files
 * 
 **/
function gasoline_framework_load(){
			
    if(is_admin()) //load admin files only in admin
        require_once(GASOLINE_FRAMEWORK_DIR.'admin/admin.php');
        
    require_once(GASOLINE_FRAMEWORK_DIR.'core/core.php');
}

gasoline_framework_load();


